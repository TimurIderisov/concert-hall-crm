CREATE TABLE "user"(
	id SERIAL PRIMARY KEY,
	login varchar(30),
	"password" varchar(30),
	priority varchar(30)
);

CREATE TABLE file_info(
	id SERIAL PRIMARY KEY,
	storage_file_name varchar(50),
	original_file_name varchar(50),
	"type" varchar(30),
	url varchar(50),
	size int
);

CREATE TABLE seat(
	id SERIAL PRIMARY KEY,
	line integer,
	place integer
);

CREATE TABLE artist(
	id SERIAL PRIMARY KEY,
	name varchar(30),
	file_info_id integer,
	CONSTRAINT file_info_id_fkey FOREIGN KEY (file_info_id)
	REFERENCES file_info(id)
);


CREATE TABLE request(
	id SERIAL PRIMARY KEY,
	content varchar(300),
	user_id integer,
	CONSTRAINT user_id_fkey FOREIGN KEY (user_id)
	REFERENCES "user"(id)
);


CREATE TABLE "comment"(
	id SERIAL PRIMARY KEY,
	content varchar(100),
	user_id integer,
	CONSTRAINT user_id_fkey FOREIGN KEY (user_id)
	REFERENCES "user"(id)
);


CREATE TABLE "event"(
	id SERIAL PRIMARY KEY,
	title varchar(50),
	description varchar(300),
	artist_id integer,
	"time" timestamp,
	file_info_id integer,
	CONSTRAINT file_info_id_fkey FOREIGN KEY (file_info_id)
	REFERENCES file_info(id),
	CONSTRAINT artist_id_fkey FOREIGN KEY (artist_id)
	REFERENCES artist(id)
);



CREATE TABLE ticket(
	id SERIAL PRIMARY KEY,
	user_id integer,
	event_id integer,
	seat_id integer,
	CONSTRAINT user_id_fkey FOREIGN KEY (user_id)
	REFERENCES "user"(id),
	CONSTRAINT event_id_fkey FOREIGN KEY (event_id)
	REFERENCES "event"(id),
	CONSTRAINT seat_id_fkey FOREIGN KEY (seat_id)
	REFERENCES seat(id)
);



INSERT INTO "user"(id, login,"password",priority) VALUES (1,'login','password','user'), (2,'login2','password2','user'), (3,'login3','password3','admin'), (4,'login','password','operator');

INSERT INTO seat VALUES (1,1,1),  (2,1,2),  (3,1,3), (4,1,4), (5,1,5), (6,1,6), (7,1,7), (8,1,8), (9,1,9), (10,1,10), (11,1,11), (12,1,12), (13,1,13), (14,1,14);

INSERT INTO artist (id, name) VALUES (1,'meladze'), (2,'salavat'), (3,'scriptonite'), (4,'ddt'), (5,'beshevli'), (6,'baskov');

INSERT INTO request  VALUES (1,'i have a problem', 1), (2,'i have a big problem', 2), (3,'i have very big problem', 2);

INSERT INTO "comment"  VALUES (1,'i like it', 1), (2,'amazing', 2), (3,'bazinga', 2);

INSERT INTO "event"  VALUES (1,'star factory', 'the best thing you`ve ever seen', 1,'2016-06-22 19:10:25-07',null), (2,'spring concert', 'the best thing you`ve ever seen too', 2,'2018-06-22 19:10:25-07',null),(3,'winter', 'the best thing you`ve ever seen #3', 3,'2016-06-03 19:10:25-07',null);

INSERT INTO ticket  VALUES (1, 1, 1), (2, 1, 2), (3, 3, 1);




