package ru.kpfu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.model.User;
import ru.kpfu.security.role.Role;

import java.util.List;
import java.util.Optional;

/**
 * Created by Timur Iderisov on 24.03.2018.
 */
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findOneByLogin(String login);
    Optional<User> findByCode(String code);
    Optional<User> findById(Integer userId);
    List<User> findAllByRole(Role role);
}
