package ru.kpfu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.model.Seat;

/**
 * Created by Timur Iderisov on 27.03.2018.
 */
public interface SeatRepository extends JpaRepository<Seat, Integer> {

}
