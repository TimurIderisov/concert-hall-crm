package ru.kpfu.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.model.FileInfo;

public interface FileInfoRepository extends JpaRepository<FileInfo, Long> {
    FileInfo findOneByStorageFileName(String storageFileName);
}