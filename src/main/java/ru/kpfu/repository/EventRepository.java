package ru.kpfu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.model.Event;

import java.util.List;

/**
 * Created by Timur Iderisov on 28.05.2018.
 */
public interface EventRepository extends JpaRepository<Event, Integer> {
    @Query(
            value = "SELECT * FROM event WHERE event.title LIKE :searchTerm",
            nativeQuery = true
    )
    List<Event> searchWithNativeQuery(@Param("searchTerm") String searchTerm);
}
