package ru.kpfu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.model.Artist;

/**
 * Created by Timur Iderisov on 27.03.2018.
 */
public interface ArtistRepository extends JpaRepository<Artist, Integer> {
}
