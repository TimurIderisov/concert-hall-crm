package ru.kpfu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.model.Event;
import ru.kpfu.model.Ticket;
import ru.kpfu.model.User;

import java.util.List;

/**
 * Created by Timur Iderisov on 27.03.2018.
 */
public interface TicketRepository extends JpaRepository<Ticket, Integer> {
    List<Ticket> findAllByUser(User user);
    List<Ticket> findAllByEvent(Event event);
}
