package ru.kpfu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.model.Comment;
import ru.kpfu.model.Event;

import java.util.List;

/**
 * Created by Timur Iderisov on 27.03.2018.
 */
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    List<Comment> findAllByEvent(Event event);

}
