package ru.kpfu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.model.Question;
import ru.kpfu.model.State;

import java.util.List;

/**
 * Created by Timur Iderisov on 28.03.2018.
 */
public interface QuestionRepository extends JpaRepository<Question,Integer> {
    List<Question> findAllByState(State state);
}
