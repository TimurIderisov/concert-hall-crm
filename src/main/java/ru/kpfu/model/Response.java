package ru.kpfu.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Setter
@Getter
public class Response {
    private String status;
    private Object data;
}