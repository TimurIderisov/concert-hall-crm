package ru.kpfu.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by Timur Iderisov on 23.03.2018.
 */

@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private State state;

    private String answer;

    private String content;
}
