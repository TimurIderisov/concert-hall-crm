package ru.kpfu.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by Timur Iderisov on 23.03.2018.
 */

@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
public class Seat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer line;
    private Integer place;
}
