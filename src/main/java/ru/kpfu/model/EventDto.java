package ru.kpfu.model;

import lombok.*;

/**
 * Created by Timur Iderisov on 23.03.2018.
 */

@Getter
@Setter
@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class EventDto {
    private Integer id;

    private String title;
    private String description;

    private String artist;

    private String time;

    private String eventPhoto;

}
