package ru.kpfu.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Timur Iderisov on 23.03.2018.
 */

@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
public class Artist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;


    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "photo_id")
    private EventPhoto eventPhoto;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "artist")
    private Set<Event> events;

}
