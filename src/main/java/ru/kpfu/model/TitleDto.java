package ru.kpfu.model;

import lombok.*;

/**
 * Created by Timur Iderisov on 23.03.2018.
 */

@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class TitleDto {
    private String title;
}
