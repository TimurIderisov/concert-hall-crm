package ru.kpfu.model;

/**
 * Created by Timur Iderisov on 05.06.2018.
 */
public enum State {
    NEW, CHECKED
}
