package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kpfu.model.User;
import ru.kpfu.service.UserService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by Timur Iderisov on 23.03.2018.
 */
@Controller
@RequestMapping("/admin")
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String getUsers(@ModelAttribute("model")ModelMap model) {

        List<User> users = userService.getAllUsers();

        model.addAttribute("users",users);
        return "all_users_page";
    }


    @RequestMapping(value = "/users/delete/{id}",method = RequestMethod.GET)
    public ModelAndView deleteUser(@PathVariable("id") Integer id){

        userService.deleteUser(id);

        ModelAndView modelAndView =  new ModelAndView("redirect:/admin/users");
        return modelAndView;

    }


    @RequestMapping(value = "/users/edit/{id}",method = RequestMethod.GET)
    public String editUser(@PathVariable("id") Integer id, @ModelAttribute("model")ModelMap model) {

        User user = userService.findUser(id);
        model.addAttribute(user);
        return "edit_user";

    }

    @RequestMapping(value = "/users/doc")
    public ResponseEntity<InputStreamResource> getDoc(){

        List<User> users = userService.getAllUsers();
        File file = userService.getListOfUsers(users);
        InputStreamResource resource = null;
        try {
            resource = new InputStreamResource(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
                .contentType(MediaType.TEXT_HTML)
                .contentLength(file.length())
                .body(resource);
    }


}
