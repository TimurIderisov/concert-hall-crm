package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kpfu.model.Question;
import ru.kpfu.service.QuestionService;

import java.util.List;

/**
 * Created by Timur Iderisov on 28.03.2018.
 */
@Controller
public class QuestionController {

    @Autowired
    QuestionService questionService;

    @GetMapping(value = "/questions")
    public String getQuestionsPage(@ModelAttribute("model")ModelMap model) {

        List<Question> questions = questionService.getAllCheckedQuestions();
        model.addAttribute("questions",questions);
        return "questions_page";

    }


    @GetMapping("/questions/{id}")
    public String getEventPage(@PathVariable("id") Integer id,@ModelAttribute("model") ModelMap model) {

        Question question = questionService.findQuestion(id);
        model.addAttribute("question",question);
        return "question_page";

    }

    @PostMapping("/user/questions")
    public String addQuestion(@ModelAttribute Question question) {
       questionService.addQuestion(question);
        return "redirect:/questions";

    }


    @GetMapping(value = "/manager/questions")
    public String getManagerQuestionsPage(@ModelAttribute("model")ModelMap model) {

        List<Question> questions = questionService.getAllNewQuestions();
        model.addAttribute("questions",questions);
        return "all_questions_page";

    }


    @GetMapping("/manager/questions/delete/{id}")
    public String deleteQuestion(@PathVariable("id") Integer id) {

        questionService.deleteQuestion(id);
        return "redirect:/manager/questions";

    }


    @GetMapping("/manager/questions/edit/{id}")
    public String answerQuestionPage(@PathVariable("id") Integer id,@ModelAttribute("model") ModelMap model) {

        Question question = questionService.findQuestion(id);
        model.addAttribute("question",question);
        return "edit_question";

    }

    @PostMapping("/manager/questions/edit")
    public String answerQuestion(@ModelAttribute Question question) {

        questionService.updateQuestion(question);
        return "redirect:/manager/questions";

    }


}
