package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.forms.TicketAddingForm;
import ru.kpfu.model.Event;
import ru.kpfu.model.Ticket;
import ru.kpfu.model.User;
import ru.kpfu.service.AuthenticationService;
import ru.kpfu.service.EventService;
import ru.kpfu.service.SeatService;
import ru.kpfu.service.TicketService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by Timur Iderisov on 05.06.2018.
 */
@Controller
public class TicketController {

    @Autowired
    TicketService ticketService;

    @Autowired
    private AuthenticationService service;

    @Autowired
    EventService eventService;

    @Autowired
    SeatService seatService;

    @GetMapping(value = "/user/tickets")
    public String getUserTicketsPage(@ModelAttribute("model") ModelMap model, Authentication authentication) {

        User user = service.getUserByAuthentication(authentication);
        List<Ticket> tickets = ticketService.getTicketsByUser(user);
        model.addAttribute("tickets", tickets);
        return "user_tickets";

    }


    @GetMapping(value = "/user/tickets/{id}")
    public String getReservePage(@ModelAttribute("model") ModelMap model, @PathVariable Integer id) {

        Event event = eventService.findEvent(id);
        model.addAttribute("event",event);
        return "reserve_ticket";

    }

    @PostMapping("/user/tickets")
    public String addTicket(@ModelAttribute TicketAddingForm form, Authentication authentication) {

        User user = service.getUserByAuthentication(authentication);
        ticketService.addTicket(form, user);
        return "redirect:/user/tickets";

    }


    @GetMapping(value = "/manager/tickets")
    public String getTicketsPage(@ModelAttribute("model") ModelMap model) {

        List<Event> events = eventService.getEvents();
        List<Ticket> tickets = ticketService.getAllTickets();
        model.addAttribute("tickets", tickets);
        model.addAttribute("events", events);
        return "all_tickets_page";

    }

    @GetMapping(value = "/manager/tickets/event")
    public String getTicketByEventPage(@ModelAttribute("model") ModelMap model, Integer eventId){

        Event event = eventService.findEvent(eventId);
        List<Event> events = eventService.getEvents();
        List<Ticket> tickets = ticketService.getTicketsByEvent(event);
        model.addAttribute("tickets", tickets);
        model.addAttribute("events", events);
        return "all_tickets_page";
    }


    @RequestMapping(value = "/user/tickets/my")
    public ResponseEntity<InputStreamResource> getTicketDoc(Authentication authentication) {

        User user = service.getUserByAuthentication(authentication);
        List<Ticket> tickets = ticketService.getTicketsByUser(user);
        File file = ticketService.getListOfTickets(tickets);
        InputStreamResource resource = null;
        try {
            resource = new InputStreamResource(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
                .contentType(MediaType.TEXT_HTML)
                .contentLength(file.length())
                .body(resource);
    }

}
