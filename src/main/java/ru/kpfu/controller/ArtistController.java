package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.forms.ArtistAddingForm;
import ru.kpfu.model.Artist;
import ru.kpfu.service.ArtistService;
import ru.kpfu.validators.ArtistAddingFormValidator;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Timur Iderisov on 27.03.2018.
 */
@Controller
public class ArtistController {

    @Autowired
    ArtistService artistService;


    @Autowired
    private ArtistAddingFormValidator artistAddingFormValidator;

    @InitBinder("artistAddingForm")
    public void initArtistFormValidator(WebDataBinder binder) {
        binder.addValidators(artistAddingFormValidator);
    }



    @GetMapping(value = "/artists")
    public String getSimpleArtistsPage(@ModelAttribute("model")ModelMap model) {

        List<Artist> artists = artistService.getAllArtists();
        model.addAttribute("artists",artists);
        return "artists_page";

    }


    @GetMapping("/artists/{id}")
    public String getEventPage(@PathVariable("id") Integer id,@ModelAttribute("model") ModelMap model) {

        Artist artist = artistService.findArtist(id);
        model.addAttribute("artist",artist);
        return "artist_page";

    }


    @GetMapping(value = "/manager/artists")
    public String getArtistsPage(@ModelAttribute("model")ModelMap model) {

        List<Artist> artists = artistService.getAllArtists();
        model.addAttribute("artists",artists);
        return "all_artists_page";

    }

    @PostMapping("/manager/artists")
    public String addNewEvent(@Valid @ModelAttribute("artistAddingForm") ArtistAddingForm artistAddingForm,
                              BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/manager/artists";
        }

        artistService.save(artistAddingForm);
        return "redirect:/manager/artists";

    }



    @GetMapping("/manager/artists/delete/{id}")
    public String deleteArtist(@PathVariable("id") Integer id) {

        artistService.deleteArtist(id);
        return "redirect:/manager/artists";

    }


    @GetMapping("/manager/artists/edit/{id}")
    public String editArtistPage(@PathVariable("id") Integer id,@ModelAttribute("model") ModelMap model) {

        Artist artist = artistService.findArtist(id);
        model.addAttribute("artist",artist);
        return "edit_artist";

    }

    @PostMapping("/manager/artists/edit")
    public String editArtist(@ModelAttribute Artist artist) {

        artistService.updateArtist(artist);
        return "redirect:/manager/artists";

    }


}
