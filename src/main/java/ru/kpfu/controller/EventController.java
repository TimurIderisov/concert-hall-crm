package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.forms.EventAddingForm;
import ru.kpfu.model.Artist;
import ru.kpfu.model.Comment;
import ru.kpfu.model.Event;
import ru.kpfu.model.User;
import ru.kpfu.service.ArtistService;
import ru.kpfu.service.AuthenticationService;
import ru.kpfu.service.CommentService;
import ru.kpfu.service.EventService;
import ru.kpfu.validators.EventAddingFormValidator;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Timur Iderisov on 12.05.2018.
 */
@Controller
public class EventController {

    @Autowired
    private EventService eventService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private AuthenticationService service;


    @Autowired
    private EventAddingFormValidator eventAddingFormValidator;

    @InitBinder("eventAddingForm")
    public void initEventFormValidator(WebDataBinder binder) {
        binder.addValidators(eventAddingFormValidator);
    }


    @Autowired
    private ArtistService artistService;


    @GetMapping("/events")
    public String getEventsPage(@ModelAttribute("model") ModelMap model) {

        List<Event> events = eventService.getEvents();
        model.addAttribute("events", events);
        return "events_page";

    }

    @GetMapping("/events/{id}")
    public String getEventPage(@PathVariable("id") Integer id, @ModelAttribute("model") ModelMap model) {

        Event event = eventService.findEvent(id);
        List<Comment> comments = commentService.getAllCommentsByEvent(event);
        model.addAttribute("event", event);
        model.addAttribute("comments", comments);
        return "event_page";

    }


    @PostMapping("/user/events/{id}")
    public String addComment(@ModelAttribute Comment comment, @PathVariable("id") Integer id, Authentication authentication) {
        if (authentication != null) {
            User user = service.getUserByAuthentication(authentication);
            comment.setUser(user);
            comment.setEvent(eventService.findEvent(id));
            commentService.addComment(comment);
        }
        return "redirect:/events/"+ id;

    }


    @GetMapping("/manager/events")
    public String getManagerEventsPage(@ModelAttribute("model") ModelMap model) {

        List<Artist> artists = artistService.getAllArtists();
        List<Event> events = eventService.getEvents();
        model.addAttribute("events", events);
        model.addAttribute("artists", artists);
        return "manager_events_page";

    }

    @PostMapping("/manager/events")
    public String addEvent(@Valid @ModelAttribute("eventAddingForm") EventAddingForm eventAddingForm,
                           BindingResult errors, RedirectAttributes attributes) {

        if (errors.hasErrors()) {
            System.out.println(errors.getAllErrors());
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/manager/events";
        }

        eventService.save(eventAddingForm);
        return "redirect:/manager/events";

    }


    @GetMapping("/manager/events/delete/{id}")
    public String deleteEvent(@PathVariable("id") Integer id) {

        eventService.deleteEvent(id);
        return "redirect:/manager/events";

    }


    @GetMapping("/manager/events/edit/{id}")
    public String editEventPage(@PathVariable("id") Integer id, @ModelAttribute("model") ModelMap model) {

        List<Artist> artists = artistService.getAllArtists();
        Event event = eventService.findEvent(id);
        model.addAttribute("event", event);
        model.addAttribute("artists", artists);
        return "edit_event";

    }

    @PostMapping("/manager/events/edit")
    public String editEvent(@ModelAttribute Event event) {

        eventService.updateEvent(event);
        return "redirect:/manager/events";

    }

}
