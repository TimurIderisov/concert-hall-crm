package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.model.*;
import ru.kpfu.service.EventService;
import ru.kpfu.service.UserService;

import java.util.List;

/**
 * Created by Timur Iderisov on 06.06.2018.
 */

@RestController
public class MyRestController {

    @Autowired
    private UserService userService;

    @Autowired
    private EventService eventService;


    @PostMapping(value = "/admin/api/users/edit")
    public Response postManager(@RequestBody User user) {

        User updatedUser = userService.findUser(user.getId());
        updatedUser.setLogin(user.getLogin());
        updatedUser.setRole(user.getRole());
        userService.updateUser(updatedUser);
        Response response = new Response("Done", user);
        return response;

    }

    @PostMapping(value = "/events/api/title")
    public Response getEventByTitlePage(@RequestBody TitleDto titleDto) {

        List<Event> events = eventService.getEventsByTitle(titleDto.getTitle());
        Response response = new Response("Bad", null);

        if(events.size() > 0){
            Event event = events.get(0);

            if(null != event){
                EventDto eventDto = new EventDto(event.getId(),event.getTitle(),event.getDescription(),
                        event.getArtist().getName(),
                        event.getTime(),
                        event.getEventPhoto().getFileInfo().getStorageFileName());

                response = new Response("Done", eventDto);
            }
        }

        return response;
    }


}