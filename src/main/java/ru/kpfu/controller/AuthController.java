package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.model.User;
import ru.kpfu.security.role.Role;
import ru.kpfu.service.AuthenticationService;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Timur Iderisov on 12.05.2018.
 */
@Controller
public class AuthController {

    @Autowired
    private AuthenticationService service;

    @GetMapping("/login")
    public String login(@ModelAttribute("model") ModelMap model, Authentication authentication,
                        @RequestParam Optional<String> error) {
        if (authentication != null) {
            User user = service.getUserByAuthentication(authentication);
            if (user.getRole().equals(Role.USER)) {
                return "redirect:/events";
            } else if (user.getRole().equals(Role.ADMIN)) {
                return "redirect:/admin/users";
            } else if(user.getRole().equals(Role.MANAGER)){
                return "redirect:/manager/events";
            }
        }
        model.addAttribute("error", error);
        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, Authentication authentication) {
        if (authentication != null) {
            request.getSession().invalidate();
        }
        return "redirect:/login";
    }

    @GetMapping("/")
    public String root(Authentication authentication) {
        if (authentication != null) {
            User user = service.getUserByAuthentication(authentication);
            if (user.getRole().equals(Role.USER)) {
                return "redirect:/events";
            } else if (user.getRole().equals(Role.ADMIN)) {
                return "redirect:/admin/users";
            }else if(user.getRole().equals(Role.MANAGER)){
                return "redirect:/manager/events";
            }
        }
        return "redirect:/login";
    }
}
