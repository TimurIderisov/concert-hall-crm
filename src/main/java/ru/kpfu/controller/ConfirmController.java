package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.kpfu.model.User;
import ru.kpfu.repository.UserRepository;
import ru.kpfu.security.details.UserDetailsServiceImpl;
import ru.kpfu.security.states.State;

@Controller
public class ConfirmController {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private UserRepository usersRepository;

    @GetMapping(value = "/confirm/{cod}")
    public String confirmUser(
            @PathVariable("cod") String cod) {
        User user = usersRepository.findByCode(cod).get();
        user.setState(State.CONFIRMED);
        usersRepository.save(user);
        System.out.println(cod);
        return "success_conf";
    }
}