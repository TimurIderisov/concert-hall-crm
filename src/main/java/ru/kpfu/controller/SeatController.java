package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kpfu.model.Seat;
import ru.kpfu.service.SeatService;

import java.util.List;

/**
 * Created by Timur Iderisov on 27.03.2018.
 */
@Controller
public class SeatController {

    @Autowired
    private SeatService seatService;


    @RequestMapping(value = "/seats", method = RequestMethod.GET)
    public ModelAndView getSeats() {

        List<Seat> seats = seatService.getAllSeats();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("seats", seats);
        modelAndView.setViewName("all_seats_page");

        return modelAndView;
    }

    @RequestMapping(value = "/seats", method = RequestMethod.POST)
    public ModelAndView addSeat(Seat seat) {
        seatService.addSeat(seat);
        ModelAndView modelAndView = new ModelAndView("redirect:/seats");
        return modelAndView;
    }

    @RequestMapping(value = "/seats/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteSeat(@PathVariable("id") Integer id) {

        seatService.deleteSeat(id);

        ModelAndView modelAndView = new ModelAndView("redirect:/seats");
        return modelAndView;

    }


    @RequestMapping(value = "/seats/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editUser(@PathVariable("id") Integer id) {

        Seat seat = seatService.findSeat(id);


        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("seat", seat);
        modelAndView.setViewName("edit_seat");

        return modelAndView;
    }


    @RequestMapping(value = "/seats/edit", method = RequestMethod.POST)
    public ModelAndView editSeat(Seat seat) {

        seatService.updateSeat(seat);

        ModelAndView modelAndView = new ModelAndView("redirect:/seats");
        return modelAndView;
    }

}
