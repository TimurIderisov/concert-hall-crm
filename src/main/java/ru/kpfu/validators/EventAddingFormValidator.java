package ru.kpfu.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.forms.EventAddingForm;

/**
 * Created by Timur Iderisov on 13.05.2018.
 */
@Component
public class EventAddingFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(EventAddingForm.class.getName());
    }

    @Override
    public void validate(Object target, Errors errors) {
        EventAddingForm form = (EventAddingForm) target;

        if(form.getPhoto().isEmpty()){
            errors.reject("bad.photo", "Empty file");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "empty.name", "Empty name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "time", "empty.time", "Empty time");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "artistId", "empty.artistId", "Empty artistId");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "empty.description", "Empty description");

    }
}
