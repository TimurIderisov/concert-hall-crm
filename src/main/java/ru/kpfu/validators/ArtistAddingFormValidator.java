package ru.kpfu.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.forms.ArtistAddingForm;

/**
 * Created by Timur Iderisov on 13.05.2018.
 */
@Component
public class ArtistAddingFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(ArtistAddingForm.class.getName());
    }

    @Override
    public void validate(Object target, Errors errors) {
        ArtistAddingForm form = (ArtistAddingForm) target;

        if(form.getPhoto().isEmpty()){
            errors.reject("bad.photo", "Empty file");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "empty.name", "Empty name");

    }
}
