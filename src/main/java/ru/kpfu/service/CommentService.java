package ru.kpfu.service;

import ru.kpfu.model.Comment;
import ru.kpfu.model.Event;

import java.util.List;

/**
 * Created by Timur Iderisov on 05.06.2018.
 */
public interface CommentService {

    List<Comment> getAllCommentsByEvent(Event event);

    void addComment(Comment comment);

}
