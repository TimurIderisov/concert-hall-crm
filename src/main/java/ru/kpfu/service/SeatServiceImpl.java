package ru.kpfu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.model.Seat;
import ru.kpfu.repository.SeatRepository;

import java.util.List;

/**
 * Created by Timur Iderisov on 27.03.2018.
 */
@Service
public class SeatServiceImpl implements SeatService {

    @Autowired
    private SeatRepository seatRepository;


    @Override
    public List<Seat> getAllSeats() {
        return (List<Seat>) seatRepository.findAll();
    }

    @Override
    public void addSeat(Seat seat) {
        seatRepository.save(seat);
    }

    @Override
    public void deleteSeat(Integer id) {
        seatRepository.delete(id);
    }

    @Override
    public Seat findSeat(Integer id) {
        return seatRepository.findOne(id);
    }

    @Override
    public void updateSeat(Seat seat) {
        seatRepository.save(seat);
    }
}
