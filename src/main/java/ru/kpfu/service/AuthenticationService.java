package ru.kpfu.service;

import org.springframework.security.core.Authentication;
import ru.kpfu.model.User;


public interface AuthenticationService {
    User getUserByAuthentication(Authentication authentication);
}
