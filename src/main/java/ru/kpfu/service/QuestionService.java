package ru.kpfu.service;

import ru.kpfu.model.Question;

import java.util.List;

/**
 * Created by Timur Iderisov on 28.03.2018.
 */
public interface QuestionService {

    List<Question> getAllQuestions();

    void addQuestion(Question question);

    void deleteQuestion(Integer id);

    Question findQuestion(Integer id);

    void updateQuestion(Question question);

    List<Question> getAllCheckedQuestions();

    List<Question> getAllNewQuestions();
}
