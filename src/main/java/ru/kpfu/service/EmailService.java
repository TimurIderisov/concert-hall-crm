package ru.kpfu.service;


public interface EmailService {
    void sendMail(String text, String subject, String email);
}