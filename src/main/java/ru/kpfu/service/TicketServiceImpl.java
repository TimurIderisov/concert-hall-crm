package ru.kpfu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.forms.TicketAddingForm;
import ru.kpfu.model.Event;
import ru.kpfu.model.Seat;
import ru.kpfu.model.Ticket;
import ru.kpfu.model.User;
import ru.kpfu.repository.EventRepository;
import ru.kpfu.repository.SeatRepository;
import ru.kpfu.repository.TicketRepository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Created by Timur Iderisov on 05.06.2018.
 */
@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    SeatRepository seatRepository;


    @Override
    public List<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }

    @Override
    public List<Ticket> getTicketsByUser(User user) {
        return ticketRepository.findAllByUser(user);
    }

    @Override
    public void addTicket(TicketAddingForm form, User user) {
        Event event = eventRepository.findOne(form.getEventId());
        Seat seat = seatRepository.findOne(form.getSeatId());
        List<Seat> seats = event.getSeats();
        seats.remove(seat);
        event.setSeats(seats);

        Ticket ticket = Ticket.builder().event(event).seat(seat).user(user).build();
        ticketRepository.save(ticket);
    }

    @Override
    public List<Ticket> getTicketsByEvent(Event event) {
        return ticketRepository.findAllByEvent(event);
    }

    @Override
    public File getListOfTickets(List<Ticket> tickets) {
        String html = "";

        for(Ticket t : tickets){
            String concat = "<h1>" + t.getEvent().getTitle() + "</h1>" + "<h4> time:" + t.getEvent().getTime() + "</h4>" +
                    "<h3> Line: " + t.getSeat().getLine()  + " Place: " + t.getSeat().getPlace() +"</h3>" +
                    "<h3> Login: " + t.getUser().getLogin() + "</h3> <br><br>";
            html += concat;
        }

        File file = new File( UUID.randomUUID().toString()+".html");

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write(html);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  file;
    }

}
