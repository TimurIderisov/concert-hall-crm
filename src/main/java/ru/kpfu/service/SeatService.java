package ru.kpfu.service;

import ru.kpfu.model.Seat;

import java.util.List;

/**
 * Created by Timur Iderisov on 27.03.2018.
 */
public interface SeatService {

    List<Seat> getAllSeats();

    void addSeat(Seat seat);

    void deleteSeat(Integer id);

    Seat findSeat(Integer id);

    void updateSeat(Seat seat);

}
