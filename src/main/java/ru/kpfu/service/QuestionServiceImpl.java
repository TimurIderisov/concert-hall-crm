package ru.kpfu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.model.Question;
import ru.kpfu.model.State;
import ru.kpfu.repository.QuestionRepository;

import java.util.List;

/**
 * Created by Timur Iderisov on 28.03.2018.
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    QuestionRepository questionRepository;

    @Override
    public List<Question> getAllQuestions() {
        return (List<Question>) questionRepository.findAll();
    }

    @Override
    public void addQuestion(Question question) {
        question.setState(State.NEW);
        questionRepository.save(question);
    }

    @Override
    public void deleteQuestion(Integer id) {
        questionRepository.delete(id);
    }

    @Override
    public Question findQuestion(Integer id) {
        return questionRepository.findOne(id);
    }

    @Override
    public void updateQuestion(Question question) {
        question.setState(State.CHECKED);
        questionRepository.save(question);
    }

    @Override
    public List<Question> getAllCheckedQuestions() {
        return questionRepository.findAllByState(State.CHECKED);
    }

    @Override
    public List<Question> getAllNewQuestions() {
        return questionRepository.findAllByState(State.NEW);
    }
}
