package ru.kpfu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import ru.kpfu.forms.UserRegistrationForm;
import ru.kpfu.model.User;
import ru.kpfu.repository.UserRepository;
import ru.kpfu.security.role.Role;
import ru.kpfu.security.states.State;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class RegistrationServiceImpl implements RegistrationService {


    public RegistrationServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private final RestTemplate restTemplate;


    @Autowired
    private EmailService emailService;

    private ExecutorService executorService = Executors.newCachedThreadPool();


    @Autowired
    private UserRepository usersRepository;

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public void register(UserRegistrationForm userForm) {
        User newUser = User.builder()
                .login(userForm.getLogin())
                .hashPassword(passwordEncoder.encode(userForm.getPassword()))
                .role(Role.USER)
                .email(userForm.getEmail())
                .state(State.NOT_CONFIRMED)
                .build();
        newUser = createUuid(userForm.getEmail(),newUser);
        usersRepository.save(newUser);
    }


    @Override
    public void registerNewManager(UserRegistrationForm userRegistrationForm) {

        User newUser = User.builder()
                .login(userRegistrationForm.getLogin())
                .hashPassword(passwordEncoder.encode(userRegistrationForm.getPassword()))
                .role(Role.MANAGER)
                .state(State.CONFIRMED)
                .build();
        usersRepository.save(newUser);

    }


    @Transactional
    @Override
    public User createUuid(String email, User user) {
        String uuid = UUID.randomUUID().toString();
        executorService.submit(() -> {
            emailService.sendMail("<p><a href=\"http://localhost:8080/confirm/"+uuid+"\">Подтвердить</a></p>",
                    "Подтвердите почту пройдя по ссылке" ,
                    email);
        });
        user.setCode(uuid);
        return user;
    }

}
