package ru.kpfu.service;

import ru.kpfu.forms.TicketAddingForm;
import ru.kpfu.model.Event;
import ru.kpfu.model.Ticket;
import ru.kpfu.model.User;

import java.io.File;
import java.util.List;

/**
 * Created by Timur Iderisov on 27.03.2018.
 */
public interface TicketService {

    List<Ticket> getAllTickets();

    List<Ticket> getTicketsByUser(User user);

    void addTicket(TicketAddingForm form, User user);

    List<Ticket> getTicketsByEvent(Event event);

    File getListOfTickets(List<Ticket> tickets);
}
