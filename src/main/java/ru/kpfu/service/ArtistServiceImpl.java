package ru.kpfu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.forms.ArtistAddingForm;
import ru.kpfu.model.Artist;
import ru.kpfu.model.EventPhoto;
import ru.kpfu.model.FileInfo;
import ru.kpfu.repository.ArtistRepository;
import ru.kpfu.util.FileStorageUtil;

import java.util.List;

/**
 * Created by Timur Iderisov on 27.03.2018.
 */
@Service
public class ArtistServiceImpl implements  ArtistService{

    @Autowired
    ArtistRepository artistRepository;


    @Autowired
    private FileStorageUtil fileStorageUtil;

    @Override
    public List<Artist> getAllArtists() {
        return (List<Artist>) artistRepository.findAll();
    }

    @Override
    public void addArtist(Artist artist) {
        artistRepository.save(artist);
    }

    @Override
    public void save(ArtistAddingForm artistAddingForm) {

        FileInfo photoFileInfo = fileStorageUtil.getEventPhotoByMultipart(artistAddingForm.getPhoto());
        EventPhoto eventPhoto = EventPhoto.builder().fileInfo(photoFileInfo).build();
        fileStorageUtil.saveEventPhotoToStorage(artistAddingForm.getPhoto(), eventPhoto);

        artistRepository.save(Artist.builder()
                .name(artistAddingForm.getName())
                .eventPhoto(eventPhoto)
                .build()
        );

    }

    @Override
    public void deleteArtist(Integer id) {
        artistRepository.delete(id);
    }

    @Override
    public Artist findArtist(Integer id) {
        return artistRepository.findOne(id);
    }

    @Override
    public void updateArtist(Artist artist) {
        artist.setEventPhoto(artistRepository.findOne(artist.getId()).getEventPhoto());
        artistRepository.save(artist);
    }
}
