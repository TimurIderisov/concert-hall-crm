package ru.kpfu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.forms.EventAddingForm;
import ru.kpfu.model.*;
import ru.kpfu.repository.ArtistRepository;
import ru.kpfu.repository.EventRepository;
import ru.kpfu.repository.SeatRepository;
import ru.kpfu.util.FileStorageUtil;

import java.util.List;

/**
 * Created by Timur Iderisov on 28.05.2018.
 */
@Service
public class EventServiceImpl implements EventService {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    SeatRepository seatRepository;


    @Autowired
    ArtistRepository artistRepository;

    @Autowired
    private FileStorageUtil fileStorageUtil;

    @Override
    public List<Event> getEvents() {
        return eventRepository.findAll();
    }

    @Override
    public void save(EventAddingForm eventAddingForm) {
        FileInfo photoFileInfo = fileStorageUtil.getEventPhotoByMultipart(eventAddingForm.getPhoto());
        EventPhoto eventPhoto = EventPhoto.builder().fileInfo(photoFileInfo).build();
        fileStorageUtil.saveEventPhotoToStorage(eventAddingForm.getPhoto(), eventPhoto);

        Artist artist = artistRepository.findOne(eventAddingForm.getArtistId());

        List<Seat> seats = seatRepository.findAll();

        eventRepository.save(Event.builder()
                .title(eventAddingForm.getTitle())
                .description(eventAddingForm.getDescription())
                .time(eventAddingForm.getTime())
                .seats(seats)
                .artist(artist)
                .eventPhoto(eventPhoto)
                .build()
        );
    }

    @Override
    public void deleteEvent(Integer id) {
        eventRepository.delete(id);
    }

    @Override
    public Event findEvent(Integer id) {
        return eventRepository.findOne(id);
    }

    @Override
    public void updateEvent(Event event) {
        Event oldEvent = eventRepository.findOne(event.getId());
        event.setArtist(oldEvent.getArtist());
        event.setTime(oldEvent.getTime());
        event.setEventPhoto(oldEvent.getEventPhoto());
        eventRepository.save(event);
    }

    @Override
    public List<Event> getEventsByTitle(String title) {
        return eventRepository.searchWithNativeQuery("%" + title + "%");
    }
}
