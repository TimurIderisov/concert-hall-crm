package ru.kpfu.service;

import ru.kpfu.model.Artist;
import ru.kpfu.forms.ArtistAddingForm;
import java.util.List;

/**
 * Created by Timur Iderisov on 27.03.2018.
 */
public interface ArtistService  {

    List<Artist> getAllArtists();

    void addArtist(Artist artist);

    void save(ArtistAddingForm eventAddingForm);

    void deleteArtist(Integer id);

    Artist findArtist(Integer id);

    void updateArtist(Artist artist);

}
