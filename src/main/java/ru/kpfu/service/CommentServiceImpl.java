package ru.kpfu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.model.Comment;
import ru.kpfu.model.Event;
import ru.kpfu.repository.CommentRepository;

import java.util.List;

/**
 * Created by Timur Iderisov on 05.06.2018.
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentRepository commentRepository;


    @Override
    public List<Comment> getAllCommentsByEvent(Event event) {
        return commentRepository.findAllByEvent(event);
    }

    @Override
    public void addComment(Comment comment) {
        commentRepository.save(comment);
    }
}
