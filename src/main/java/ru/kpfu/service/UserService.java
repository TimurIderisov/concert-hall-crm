package ru.kpfu.service;

import ru.kpfu.model.User;

import java.io.File;
import java.util.List;

/**
 * Created by Timur Iderisov on 23.03.2018.
 */
public interface UserService {

    List<User> getAllUsers();

    void addUser(User user);

    void deleteUser(Integer id);

    User findUser(Integer id);

    void updateUser(User user);

    File getListOfUsers(List<User> users);
}
