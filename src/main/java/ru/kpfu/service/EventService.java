package ru.kpfu.service;

import ru.kpfu.forms.EventAddingForm;
import ru.kpfu.model.Event;

import java.util.List;

/**
 * Created by Timur Iderisov on 28.05.2018.
 */
public interface EventService {
    List<Event> getEvents();

    void save(EventAddingForm eventAddingForm);

    void deleteEvent(Integer id);

    Event findEvent(Integer id);

    void updateEvent(Event event);

    List<Event> getEventsByTitle(String title);
}
