package ru.kpfu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.model.User;
import ru.kpfu.repository.UserRepository;
import ru.kpfu.security.role.Role;
import ru.kpfu.security.states.State;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Created by Timur Iderisov on 24.03.2018.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAllUsers() {
       return (List<User>) userRepository.findAll();
    }

    @Override
    public void addUser(User user){
        user.setRole(Role.MANAGER);
        user.setState(State.CONFIRMED);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        userRepository.delete(id);
    }

    @Override
    public User findUser(Integer id) {
        return userRepository.findOne(id);
    }

    @Override
    public void updateUser(User user) {
        userRepository.save(user);
    }

    @Override
    public File getListOfUsers(List<User> users) {
        String html = "";

        for(User t : users){
            String concat = "<h1>" + t.getLogin() + "</h1>" + "<h4> id:" + t.getId() + "</h4>" +
                    "<h3> Role: " + t.getRole() + " State: " + t.getState() +"</h3>" +
                    "<h3> E-mail: " + t.getEmail() + "</h3> <br><br>";
            html += concat;
        }

        File file = new File( UUID.randomUUID().toString()+".html");

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write(html);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  file;
    }
}
