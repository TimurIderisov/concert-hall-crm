package ru.kpfu.service;

import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.forms.UserRegistrationForm;
import ru.kpfu.model.User;

public interface RegistrationService {
    void register(UserRegistrationForm userForm);

    @Transactional
    User createUuid(String email, User user);

    void registerNewManager(UserRegistrationForm userRegistrationForm);
}
