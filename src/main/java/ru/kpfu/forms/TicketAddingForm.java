package ru.kpfu.forms;

import lombok.*;

/**
 * Created by Timur Iderisov on 13.05.2018.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TicketAddingForm {
    private Integer eventId;
    private Integer seatId;
}
