package ru.kpfu.security.states;

public enum State {
    CONFIRMED, NOT_CONFIRMED, BANNED, DELETED
}