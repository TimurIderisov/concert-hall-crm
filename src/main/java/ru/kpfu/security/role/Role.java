package ru.kpfu.security.role;

public enum Role {
    ADMIN, USER, MANAGER
}
