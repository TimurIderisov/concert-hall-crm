
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
        <ul class="nav sidebar-nav">
            <li class="sidebar-brand">
                <a href="/events">
                    Events
                </a>
            </li>
            <li>
                <a href="/questions">Questions</a>
            </li>
            <li>
                <a href="/artists">Artists</a>
            </li>
            <li>
                <a href="/user/tickets">My tickets</a>
            </li>
            <li>
                <a href="/logout">Log out</a>
            </li>
        </ul>
    </nav>





