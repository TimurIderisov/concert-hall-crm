<#ftl encoding='UTF-8'>
${model.event.title}
<form method="post" action="/user/tickets">

    <input type="hidden" value="${model.event.id}" name="eventId">

    <select name="seatId" id="seatId">
    <#list model.event.seats as seat>
        <option id="${seat?index}" value=${seat.id}>${seat.line} ${seat.place}</option>
    </#list>
    </select>

    <input type="submit" value="Reserve ticket">
</form>

