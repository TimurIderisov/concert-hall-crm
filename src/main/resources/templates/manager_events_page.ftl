<#ftl encoding='UTF-8'>
<html>
<head>
    <title>Events</title>
</head>
<body>
<h1>Events</h1>

<br>
<form method="post" action="/manager/events" enctype="multipart/form-data">
    <input type="text" name="title">
    <input type="date" name="time">
    <input type="text" name="description">

    <select name="artistId" id="artistId">
    <#list model.artists as artist>
        <option id="${artist?index}" value=${artist.id}>${artist.name}</option>
    </#list>
    </select>

    <input id="file" type="file" name="photo" accept="image/*"/>

    <input type="submit" value="добавить">
</form>
<br>
<table>
    <tr>
        <th>title</th>
        <th>description</th>
        <th>artist</th>
        <th>time</th>
        <th>photo</th>
        <th>options</th>
    </tr>

<#list model.events as event>
    <tr>
        <td>${event.title}</td>
        <td>${event.description}</td>
        <td>${event.artist.name}</td>
        <td>${event.time}</td>
        <td> <img src="/files/${event.eventPhoto.fileInfo.storageFileName}"></td>
        <td>
            <a href="/manager/events/delete/${event.id}">DELETE</a>
            <a href="/manager/events/edit/${event.id}">EDIT</a>
        </td>
    </tr>
</#list>

</table>

</body>
</html>
