<#ftl encoding='UTF-8'>
<html>
<head>
    <title>Artists</title>
</head>
<body>
<h1>Artists</h1>

<br>
<form method="post" action="/manager/artists" enctype="multipart/form-data">
    <input type="text" name="name">

    <input id="file" type="file" name="photo" accept="image/*"/>

    <input type="submit" value="Add artist">
</form>
<br>
<table>
    <tr>
        <th>name</th>
        <th>photo</th>
        <th>Options</th>
    </tr>

    <#list model.artists as artist>
        <tr>
            <td>${artist.name}</td>
            <td> <img src="/files/${artist.eventPhoto.fileInfo.storageFileName}"></td>
            <td>
                <a href="/manager/artists/delete/${artist.id}">DELETE</a>
                <a href="/manager/artists/edit/${artist.id}">EDIT</a>
            </td>
        </tr>
    </#list>

</table>

</body>
</html>
