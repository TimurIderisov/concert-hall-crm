<html>
<head>
    <title>User edit</title>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="/bootstrap/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
</head>
<body>
<form id="userForm">
    <input type="hidden" value="${model.user.id}" name="id" id="id">
    <input type="text" value="${model.user.login}" name="login" id="login">
    <input type="text" value="${model.user.role}" name="role" id="role">
    <input type="submit">
</form>
<div class="col-sm-7" id="postResultDiv">
</div>

<script type="text/javascript"
        src="webjars/jquery/2.2.4/jquery.min.js"></script>

<script type="text/javascript">
    $( document ).ready(function() {

        // SUBMIT FORM
        $("#userForm").submit(function(event) {
            // Prevent the form from submitting via the browser.
            event.preventDefault();
            ajaxPost();
        });


        function ajaxPost(){

            // PREPARE FORM DATA
            var formData = {
                login : $("#login").val(),
                role :  $("#role").val(),
                id : $("#id").val()
            }

            // DO POST
            $.ajax({
                type : "POST",
                contentType : "application/json",
                url : "/admin/api/users/edit",
                data : JSON.stringify(formData),
                dataType : 'json',
                success : function(result) {
                    if(result.status == "Done"){
                        $("#postResultDiv").html("<p style='background-color:#7FA7B0; color:white; padding:20px 20px 20px 20px'>" +
                                "Post Successfully! <br>" +
                                "---> User`s Info: Login = " +
                                result.data.login + " ,Role = " + result.data.role + "</p>");
                    }else{
                        $("#postResultDiv").html("<strong>Error</strong>");
                    }
                    console.log(result);
                },
                error : function(e) {
                    alert("Error!")
                    console.log("ERROR: ", e);
                }
            });

            // Reset FormData after Posting
            resetData();

        }

        function resetData(){
            $("#login").val("");
            $("#role").val("");
        }
    })
</script>

</body>
</html>