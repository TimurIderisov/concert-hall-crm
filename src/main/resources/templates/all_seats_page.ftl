<#ftl encoding='UTF-8'>

<html>
<head>
    <title>Seats</title>
</head>
<body>
<table>
    <tr>
        <th>line</th>
        <th>place</th>
        <th>Options</th>
    </tr>
        <#list model.seats as seat>
        <tr>
            <td>${seat.line}</td>
            <td>${seat.place}</td>
            <td>
                <a href="/seats/delete/${seat.id}">DELETE</a>
                <a href="/seats/edit/${seat.id}">EDIT</a>
            </td>
        </tr>
        </#list>
</table>
<form method="post" action="/seats">
    <input type="text" name="line">
    <input type="text" name="place">
    <input type="submit">
</form>
</body>
</html>
