<#ftl encoding='UTF-8'>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="/bootstrap/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="/bootstrap/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="/css/styles.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>

<div id="wrapper">
    <div class="overlay"></div>

<#include "side.ftl">

    <div id="page-content-wrapper">
        <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
            <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
        </button>
        <div class="container">

<div style="width: 80%; height: 300px; background-color: white; opacity: 20; margin-left: 10%; margin-top:20px;"
     class="row">

    <div class="col-md-6">

        <img style="width: 95%; padding-top: 10px; max-height: 270px"
             src="/files/${model.artist.eventPhoto.fileInfo.storageFileName}">

    </div>

    <div class="col-md-4" style="margin-right: 10px; height: 300px">
        <h2>${model.artist.name}</h2>
        <br>

    </div>


</div>


        </div>
    </div>


</div>

<script src="/js/menu.js"></script>

</body>