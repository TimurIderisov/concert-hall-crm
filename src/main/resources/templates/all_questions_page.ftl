<#ftl>
<html>
<head>
    <title>Questions</title>
</head>
<body>
<table>
    <tr>
        <th>content</th>
        <th>options</th>
    </tr>
        <#list model.questions as question>
        <tr>
            <td>${question.content}</td>
            <td>
                <a href="/manager/questions/delete/${question.id}">DELETE</a>
                <a href="/manager/questions/edit/${question.id}">ANSWER</a>
            </td>
        </tr>
        </#list>
</table>

</body>
</html>
