<#ftl encoding='UTF-8'>
<html>
<head>
    <title>Event edit</title>
</head>
<body>
<form method="post" action="/manager/events/edit">
    <input type="hidden" value="${model.event.id}" name="id">
    <input type="text" value="${model.event.title}" name="title">
    <input type="text" value="${model.event.description}"  name="description">

    <input type="submit" value="edit">
</form>
</body>
</html>
