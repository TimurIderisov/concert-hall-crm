<#ftl encoding='UTF-8'>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="/bootstrap/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="/bootstrap/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="/css/styles.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
Choose event
<form action="/manager/tickets/event/">
    <select name="eventId" id="eventId">
    <#list model.events as event>
        <option id="${event?index}" value=${event.id}>${event.title}</option>
    </#list>
    </select>
    <input type="submit">
</form>

        <#list model.tickets as ticket>

            <div style="width: 80%; height: 300px; background-color: white; opacity: 20; margin-left: 10%; margin-top:20px;"
                 class="row">

                <div class="col-md-6">
                    ряд
                    <h3>${ticket.seat.line}</h3>
                    место
                    <h3>${ticket.seat.place}</h3>
                </div>

                <div class="col-md-4" style="margin-right: 10px; height: 300px">
                    <h2>${ticket.event.title}</h2>
                    <br>
                    <h3>${ticket.event.time}</h3>
                    <br>

                </div>


            </div>

        </#list>



</body>