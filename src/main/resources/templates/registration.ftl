<#ftl encoding='UTF-8'>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <STYLE>
        body {
            background-image: url("https://lh3.googleusercontent.com/5Vk3gQ9Tx31JapQMeILsyimryOey0jWk_rFJ9lE85CNBHScjUNQy5nhcPoNYTBlgUA=h556");
            background-position: center;
            background-size: cover;
        }

        .container {
            margin-top: 200px;
        }
    </STYLE>
</head>
<body>
<#if error??>
<div class="alert alert-danger" role="alert">${error}</div>
</#if>

<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Registration</h3>
                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" action="/registration" method="post">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Login" name="login" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="E-mail" name="email" type="email">
                            </div>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Sign Up">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>