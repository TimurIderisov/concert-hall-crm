<#ftl encoding='UTF-8'>
<html>
<head>
    <title>Users</title>
</head>
<body>

<h3>Add Manager</h3>
<form accept-charset="UTF-8" action="/admin/registration" method="post">
    <input placeholder="Login" name="login" type="text">
    <input placeholder="Password" name="password" type="password">
    <input type="submit" value="Add">
</form>

<h3>Users table</h3>
<a href="/admin/users/doc">Get Doc</a>

<table>
    <tr>
        <th>Login</th>
        <th>Role</th>
        <th>Options</th>
    </tr>
<#list model.users as user>
    <tr>
        <td>${user.login}</td>
        <td>${user.role}</td>
        <td>
            <a href="/admin/users/delete/${user.id}">DELETE</a>
            <a href="/admin/users/edit/${user.id}">EDIT</a>
        </td>
    </tr>
</#list>
</table>



</body>
</html>
