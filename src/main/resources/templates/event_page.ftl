<#ftl encoding='UTF-8'>

<div style="width: 80%; height: 300px; background-color: white; opacity: 20; margin-left: 10%; margin-top:20px;"
     class="row">

    <div class="col-md-6">

        <div style="font-family: Consolas">
        ${model.event.time}
        </div>

        <img style="width: 95%; padding-top: 10px; max-height: 270px"
             src="/files/${model.event.eventPhoto.fileInfo.storageFileName}">

    </div>

    <div class="col-md-4" style="margin-right: 10px; height: 300px">
        <h2>${model.event.title}</h2>
        <br>
        <div style="">
        ${model.event.description}
        </div>
    ${model.event.artist.name}

    </div>

    <a href="/user/tickets/${model.event.id}">reserve ticket</a>

    <form method="post" action="/user/events/${model.event.id}" >
        <input type="text" name="content">

    </form>


<#list model.comments as comment>
    <h3>${comment.user.id}</h3>
    <h4>${comment.content}</h4>
</#list>

</div>
