<#ftl encoding='UTF-8'>
<html>
<head>
    <title>Artist edit</title>
</head>
<body>
<form method="post" action="/manager/artists/edit">
    <input type="hidden" value="${model.artist.id}" name="id">
    <input type="text" value="${model.artist.name}" name="name">
    <input type="submit">
</form>
</body>
</html>
