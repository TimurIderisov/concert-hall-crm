<#ftl encoding='UTF-8'>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <STYLE>
        body {
            background-image: url("https://lh3.googleusercontent.com/5Vk3gQ9Tx31JapQMeILsyimryOey0jWk_rFJ9lE85CNBHScjUNQy5nhcPoNYTBlgUA=h556");
            background-position: center;
            background-size: cover;
            background-attachment: fixed;
            scroll-behavior: smooth;
            overflow-x: hidden;
        }
    </STYLE>
    <link href="/bootstrap/bootstrap-3.3.2-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="/bootstrap/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="/css/styles.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
<div id="wrapper">
    <div class="overlay"></div>

<#include "side.ftl">

    <div id="page-content-wrapper">
        <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
            <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
        </button>
        <div class="container">

            <form id="titleDtoForm">
                <input type="text" value="" name="title" id="title">
                <input type="submit">
            </form>

            <div class="col-sm-7" id="postResultDiv">
            </div>

            <div id="list">
            <#list model.events as event>

                <div style="width: 80%; height: 300px; background-color: white; opacity: 20; margin-left: 10%; margin-top:20px;"
                     class="row">

                    <div class="col-md-6">

                        <div style="font-family: Consolas">
                        ${event.time}
                        </div>

                        <img style="width: 95%; padding-top: 10px; max-height: 270px"
                             src="/files/${event.eventPhoto.fileInfo.storageFileName}">

                    </div>

                    <div class="col-md-4" style="margin-right: 10px; height: 300px">
                        <a href="/events/${event.id}"><h2>${event.title}</h2></a>
                        <br>
                        <div style="">
                        ${event.description}
                        </div>
                    ${event.artist.name}

                    </div>


                </div>

            </#list>
            </div>

        </div>
    </div>


</div>

<script src="/js/menu.js"></script>


<script type="text/javascript"
        src="webjars/jquery/2.2.4/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        // SUBMIT FORM
        $("#titleDtoForm").submit(function (event) {
            // Prevent the form from submitting via the browser.
            event.preventDefault();
            ajaxPost();
        });


        function ajaxPost() {

            // PREPARE FORM DATA
            var formData = {
                title: $("#title").val()
            }

            // DO POST
            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/events/api/title",
                data: JSON.stringify(formData),
                dataType: 'json',
                success: function (result) {
                    if (result.status == "Done") {
                        $("#postResultDiv").html("<div style='width: 80%; height: 300px; background-color: white; opacity: 20; margin-left: 10%; margin-top:20px;' class='row'><div class='col-md-6'><div style='font-family: Consolas'>" + result.data.time + "</div>" + "<img style='width: 95%; padding-top: 10px; max-height: 270px' src='/files/" + result.data.eventPhoto + "'></div><div class='col-md-4' style='margin-right: 10px; height: 300px'><a href='/events/" + result.data.id + "'><h2>" + result.data.title + "</h2></a><br><div>" + result.data.description + "</div>" + result.data.artist + "</div></div>");
                        $("#list").hide();
                    } else {
                        $("#postResultDiv").html("<strong>Error</strong>");
                    }
                    console.log(result);
                },
                error: function (e) {
                    alert("Error!")
                    console.log("ERROR: ", e);
                }
            });

            // Reset FormData after Posting
            resetData();

        }

        function resetData() {
            $("#title").val("");
        }
    })
</script>

</body>
</html>